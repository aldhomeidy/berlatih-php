<?php
function ubah_huruf($string)
{
    $huruf = [
        "a", "b", "c", "d", "e", "f",
        "g", "h", "i", "j", "k", "l",
        "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x",
        "y", "z", "a"
    ];
    $result = "";
    if (ctype_lower($string)) {
        for ($j = 0; $j < strlen($string); $j++) {
            for ($i = 0; $i < count($huruf) - 1; $i++) {
                if ($string[$j] == $huruf[$i]) {
                    $result .= $huruf[$i + 1];
                }
            }
        }
        return $result . "<br>";
    } else {
        echo "Kata-kata harus berupa lower case";
        return $result . "<br>";
    }
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
